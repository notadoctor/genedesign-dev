#
# GeneDesign module for codon analysis and manipulation
#

=head1 NAME

GeneDesign::CodonsOld

=head1 VERSION

Version 5.50

=head1 DESCRIPTION

GeneDesign functions for codon analysis and manipulation

=head1 AUTHOR

Sarah Richardson <SMRichardson@lbl.gov>.

=cut

package Bio::GeneDesign::CodonsOld;
require Exporter;

use Bio::GeneDesign::Basic qw(:GD);
use List::Util qw(max first);
use Carp;

use strict;
use warnings;

our $VERSION = 5.50;

use base qw(Exporter);
our @EXPORT_OK = qw(
  _find_in_frame
  is_ORF
  $VERSION
);
our %EXPORT_TAGS =  ( GD => \@EXPORT_OK );



=head2 _filter_for_peptide()

=cut

sub _filter_for_peptide
{
  my ($list, $pepseq, $codon_t) = @_;
  my $revcodon_t = _reverse_codon_table($codon_t);
  my %new = ();
  foreach my $seq (@$list)
  {
    
  }
  return \%new;
}



=head2 pattern_finder

=cut

sub pattern_finder
{
  # has test in t/02-codons.t
  my ($strand, $pattern, $swit, $frame, $codon_t) = @_;
  my @positions = ();
  if ($swit == 2)
  {
    return if (! $frame || ! $codon_t);
    $strand = _translate($strand, $frame, $codon_t)
  }
  my $exp = _regres($pattern, $swit);
  while ($strand =~ /(?=$exp)/ig)
  {
    push @positions, (pos $strand);
  }
  return @positions;
}

=head2 is_ORF()

takes a nucleotide sequence and a codon table and determines whether or not
the nucleotide sequence is a simple ORF - that is, starts with an ATG codon
and contains no stop codons in the first frame until the very end.

  in: nucleotide sequence (string),
      codon table (hash reference),
  out: 1 if ORF, 0 if not
  
=cut

sub is_ORF
{
  my ($nucseq, $codon_t) = @_;
  my @war2 = pattern_finder($nucseq, "*", 2, 1, $codon_t);
  return 0 if (scalar(@war2) > 1);
  return 0 if (scalar(@war2) &&
               ($war2[0] + 1) != length(_translate($nucseq, 1, $codon_t)));
  return 1;
}

=head2 compareseqs

=cut

sub compareseqs
{
  my ($cur, $tar) = @_;
  return 1 if ($tar =~ _regres($cur, 1) || $cur =~ _regres($tar, 1));
  return 0;
}

=head2 RSCU_filter()

Deletes anything from the RSCU_TABLE that doesn't meet minimum RSCU.

  in: rscu table (hash reference),
      minimum RSCU value (integer)
  out: rscu table (hash reference)
  #NO UNIT TEST
  
=cut

sub RSCU_filter
{
  my ($RSCU_TABLE, $min_value) = @_;
  my @bad_codons = grep { $RSCU_TABLE->{$_}  < $min_value }
           keys %$RSCU_TABLE;
  delete @$RSCU_TABLE{@bad_codons};
  return $RSCU_TABLE;
}

=head2 orf_finder()

=cut

sub orf_finder
{
  my ($strand, $codon_t) = @_;
  my $answer = [];
  for my $frame (qw(1 2 3 -1 -2 -3))
  {
    my $strandaa = _translate($strand, $frame, $codon_t);
    my $leng = length($strandaa);
    my $curpos = 0;
    my $orflength = 0;
    my $onnaorf = 0;
    my $orfstart = 0;
    while ($curpos <= $leng)
    {
      my $aa = substr($strandaa, $curpos, 1);
      if ($aa eq 'M' && $onnaorf eq '0')
      {
        $onnaorf = 1;
        $orfstart = $curpos;
      }
      if ($aa eq '*' || ($curpos == $leng && $onnaorf == 1))
      {
        $onnaorf= 0;
        push @$answer, [$frame, $orfstart, $orflength] if ($orflength >= .1*($leng));
        $orflength = 0;
      }
      $curpos++;
      $orflength++ if ($onnaorf == 1);
    }
  }
  return $answer;
}

1;

__END__

=head1 COPYRIGHT AND LICENSE

Copyright (c) 2013, GeneDesign developers
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

* The names of Johns Hopkins, the Joint Genome Institute, the Lawrence Berkeley
National Laboratory, the Department of Energy, and the GeneDesign developers may
not be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE DEVELOPERS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=cut